#!/usr/bin/env python2
from subprocess import check_output, call
import re
import hashlib
import time

def checkit(name, key):
    done = False
    oldhash=""
    me = re.compile(".*%s.*" % name)

    while not done:
        output = check_output("wget --no-cookies --header \"Cookie: cosign-projects.inf.ed.ac.uk=%s\"\
                               https://projects.inf.ed.ac.uk/ug4/2012/project_allocations.php --no-check-certificate -O - --quiet" % key
                             , shell=True
                             )
        newhash = hashlib.sha224(output).hexdigest()
        if newhash != oldhash:
            oldhash = newhash
            print newhash
            if me.search(output):
                print "Found, exiting"
                call("notify-send \"You're up!\"", shell=True)
                done = True
                break
            else:
                call("notify-send \"It changed\"", shell=True)
        time.sleep(60)

if __name__=='__main__':
    from sys import argv

    args = argv

    name = args[1]
    key  = args[2]

    checkit(name, key)
